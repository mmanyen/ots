# WBI Off the Shelf Libray 
This repository is for setting up a local system with all the OpenSource libraries we use. The idea would be for the local to clone this and point the compiler to the lib and include directories. It should not be nessassary for the user to build the repository as the library and header files are committed. If it is nessassary to build (say to update), the standard CMake process is used:
```
mkdir build && cd build
cmake ..
cmake --build . --target install --config Release
cmake --build . --target install --config Debug 
```

| Module | Description | Location |
| ------ | ----------- |----|
|Google Test/Mock|Unit testing|[Official Github (master)](https://github.com/google/googletest.git)|
|freetype2|Font processing|[Official git repo (master)](git://git.sv.nongnu.org/freetype/freetype2.git)|
|tinyxml2|XML handling|[Github (master)](https://github.com/leethomason/tinyxml2.git)|
|lua|Lua scripting|[Mirror (Github - master)](https://github.com/lua/lua.git)|
|sqlite3|Database|[Official distro (.zip)](https://sqlite.org/2019/sqlite-amalgamation-3280000.zip)|
|zlib|Compression|[Official distro (.zip)](https://zlib.net/zlib1211.zip)|

